import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View, TouchableOpacity, Button } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { commonStyles } from '../style';
import { navigationStore } from '../services/navigationStore';
import { drawerStore } from '../services/drawerStore';
import { GradientHeader } from '../components/GradientHeader';
import { SearchStore, SearchInput, SearchButton } from '../components/Search';
import { ShipmentsListCounterStore } from './ShipmentsListCounterStore';
import { ShipmentsListCounter } from './ShipmentsListCounter';

export const shipmentsListSearchStore = new SearchStore();

export const shipmentsListCounterStore = new ShipmentsListCounterStore();

export class ShipmentsListScreen extends React.Component {
  static propTypes = {
    navigation: PropTypes.any.isRequired,
  };

  static navigationOptions = ({ navigation }) => ({
    title: 'Shipments list',
    header: (props) => <GradientHeader {...props}/>,
    headerStyle: {
      backgroundColor: 'transparent',
    },
    headerTitleStyle: {
      color: 'white',
    },
    headerLeft: (
      <TouchableOpacity onPress={drawerStore.toggle}>
        <Icon name="menu" color="white" size={24} style={commonStyles.marginLeftSmall}/>
      </TouchableOpacity>
    ),
    headerTitle: <SearchInput searchStore={shipmentsListSearchStore} otherwise="Shipments list"/>,
    headerRight: <SearchButton searchStore={shipmentsListSearchStore}/>,
  });

  state = {
    shipments: [
      { id: 'RSQ110293E8902', address: '123 6th St.  Melbourne, FL 32904', timestamp: '04/09/2018' },
      { id: 'MWS2094709123', address: '44 Shirley Ave.  West Chicago, IL 60185', timestamp: '02/05/2018' },
    ],
  };

  handleShipment = (shipment) => () => {
    navigationStore.push('ShipmentScreen', { shipmentId: shipment.id });
  };

  render() {
    const { shipments } = this.state;
    return (
      <View style={commonStyles.screen}>
        <View style={[commonStyles.flex, commonStyles.padding]}>
          {shipments.map((shipment) => (
            <TouchableOpacity 
              style={[commonStyles.card, commonStyles.padding, commonStyles.marginBottom]} 
              key={shipment.id} 
              onPress={this.handleShipment(shipment)}
            >
              <View style={[commonStyles.row, commonStyles.marginBottomSmall]}>
                <Text style={[commonStyles.largeText, commonStyles.flex]}>{shipment.id}</Text>
                <Text style={[commonStyles.smallText, commonStyles.lightGrayText]}>{shipment.timestamp}</Text>
              </View>
              <Text style={[commonStyles.regularText, commonStyles.grayText]}>{shipment.address}</Text>
            </TouchableOpacity>
          ))}
        </View>
        <ShipmentsListCounter counterStore={shipmentsListCounterStore}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({

});