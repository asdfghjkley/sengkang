import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, TouchableOpacity, Image, Text } from 'react-native';
import { observer } from 'mobx-react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import LinearGradient from 'react-native-linear-gradient';
import { green, yellow, red, accentColor, backgroundColor, borderColor, textColor, subheaderColor, fontSize, shadow } from '../style';

@observer
export class ShipmentsListCounter extends React.Component {
  static propTypes = {
    counterStore: PropTypes.object.isRequired,
  };

  render() {
    const { counterStore } = this.props;
    return (
      <View style={styles.container}>
        <LinearGradient colors={['#333333', '#000000']} style={styles.buttons}>
          <TouchableOpacity onPress={counterStore.start}>
            <View style={[styles.button, styles.startButton]}>
              <Icon name="play-arrow" color={green} size={50}/>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={counterStore.stop}>
            <View style={[styles.button, styles.stopButton]}>
              <Icon name="stop" color={red} size={50}/>
            </View>
          </TouchableOpacity>
        </LinearGradient>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    overflow: 'visible',
  },
  buttons: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    backgroundColor: 'black',
    ...shadow,
  },
  button: {
    // flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 8,
    width: 64,
    height: 64,
    borderRadius: 32,
    borderWidth: 2,
  },
  startButton: {
    borderColor: green,
  },
  pauseButton: {
    borderColor: yellow,
  },
  stopButton: {
    borderColor: red,
  },
});