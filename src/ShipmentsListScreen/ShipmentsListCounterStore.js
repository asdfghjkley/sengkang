import { observable } from 'mobx';

export class ShipmentsListCounterStore {
  @observable isStarted = false;

  start = () => {
    this.isStarted = true;
  };

  stop = () => {
    this.isStarted = false;
  };
}
