export type Item = {
  id: string,
  description: string,
  picture: string,
  cost: string,
};

export type Shipment = {
  id: string,
  timestamp: number,
  address: string,
  items: Item[],
};

export type Operation = {
  id: string,
  timestamp: number,
  status: string,
};
