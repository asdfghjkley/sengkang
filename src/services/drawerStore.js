import { observable } from 'mobx';

export class DrawerStore {

  @observable isOpen = false;

  toggle = () => {
    this.isOpen = !this.isOpen;
  };
}

export const drawerStore = new DrawerStore();