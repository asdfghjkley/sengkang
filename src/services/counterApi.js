export server = __DEV__ ? 
  'http://192.168.200.100:8000/api/v1'
  :
  'http://192.168.200.113:8000/api/v1';

const headers = {
  'Content-Type': 'application/json',
  Accept: 'application/json',
};

export const countPalletizingBags = () => {
  return fetch(`${server}/sensor/count?name=S21`, { 
    method: 'GET', 
    headers: headers, 
  })
  .then((response) => response.json());
};

export const connect = () => {
  return fetch(`${server}/conveyor/1/connect`, { 
    method: 'GET', 
    headers: headers, 
  })
  .then((response) => response.json());
};

export const disconnect = () => {
  return fetch(`${server}/conveyor/1/disconnect`, { 
    method: 'GET', 
    headers: headers, 
  })
  .then((response) => response.json());
};

export const start = () => {
  return fetch(`${server}/conveyor/1/start`, { 
    method: 'GET', 
    headers: headers, 
  })
  .then((response) => response.json());
};

export const stop = () => {
  return fetch(`${server}/conveyor/1/stop`, { 
    method: 'GET', 
    headers: headers, 
  })
  .then((response) => response.json());
};

export const pause = () => {
  return fetch(`${server}/conveyor/1/pause`, { 
    method: 'GET', 
    headers: headers, 
  })
  .then((response) => response.json());
};

export const resume = () => {
  return fetch(`${server}/conveyor/1/resume`, { 
    method: 'GET', 
    headers: headers, 
  })
  .then((response) => response.json());
};

export const reset = () => {
  return fetch(`${server}/conveyor/1/reset`, { 
    method: 'GET', 
    headers: headers, 
  })
  .then((response) => response.json());
};

export const transfer = () => {
  return fetch(`${server}/conveyor/1/transfer`, { 
    method: 'GET', 
    headers: headers, 
  })
  .then((response) => response.json());
};

export const flush = () => {
  return fetch(`${server}/conveyor/1/flush-pallet`, { 
    method: 'GET', 
    headers: headers, 
  })
  .then((response) => response.json());
};

export const clear = () => {
  return fetch(`${server}/conveyor/1/clear`, { 
    method: 'GET', 
    headers: headers, 
  })
  .then((response) => response.json());
};