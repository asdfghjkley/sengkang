import { observable } from 'mobx';
import { NavigationActions } from 'react-navigation'; 

export class NavigationStore {

  @observable navigator = null;

  @observable currentScreen = '';

  navigate = (routeName, params) => {
    this.currentScreen = routeName;
    const action = NavigationActions.reset({ 
      index: 0, 
      actions: [NavigationActions.navigate({ routeName, params })] 
    }); 
    this.navigator.dispatch(action); 
  };

  push = (routeName, params) => {
    this.currentScreen = routeName; 
    const action = NavigationActions.navigate({ 
      type: 'Navigation/NAVIGATE', 
      routeName, 
      params, 
    }); 
    this.navigator.dispatch(action); 
  };
}

export const navigationStore = new NavigationStore();
