import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View, TouchableOpacity, Button } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { drawerStore } from '../services/drawerStore';
import { green, red, commonStyles } from '../style';
import { GradientHeader } from '../components/GradientHeader';
import { SearchStore, SearchInput, SearchButton } from '../components/Search';

const styles = StyleSheet.create({
  alignItemsCenter: {
    alignItems: 'center'
  },
  textAlignCenter: {
    textAlign: 'center',
  }
});

export const operationsHistorySearchStore = new SearchStore();

const statuses = {
  delivered: (
    <View style={[commonStyles.row, styles.alignItemsCenter]}>
      <Icon name="done" color={green} size={22} style={commonStyles.marginRightSmall}/>
      <Text style={[commonStyles.regularText, commonStyles.grayText]}>Delivered</Text>
    </View>
  ),
  cancelled: (
    <View style={[commonStyles.row, styles.alignItemsCenter]}>
      <Icon name="clear" color={red} size={22} style={commonStyles.marginRightSmall}/>
      <Text style={[commonStyles.regularText, commonStyles.grayText]}>Cancelled</Text>
    </View>
  ),
};

export class OperationsHistoryScreen extends React.Component {
  static propTypes = {
    navigation: PropTypes.any.isRequired,
  };

  static navigationOptions = ({ navigation }) => ({
    title: 'Operations history',
    header: (props) => <GradientHeader {...props}/>,
    headerTintColor: 'white',
    headerStyle: {
      backgroundColor: 'transparent',
    },
    headerTitleStyle: {
      color: 'white',
    },
    headerLeft: (
      <TouchableOpacity onPress={drawerStore.toggle}>
        <Icon name="menu" color="white" size={24} style={commonStyles.marginLeftSmall}/>
      </TouchableOpacity>
    ),
    headerTitle: <SearchInput searchStore={operationsHistorySearchStore} otherwise='Operations history'/>,
    headerRight: <SearchButton searchStore={operationsHistorySearchStore}/>,
  });

  state = {
    operations: [
      { id: 'RSQ110293E8902', status: 'delivered', timestamp: '02/05/2018' },
      { id: 'MWS2094709123', status: 'cancelled', timestamp: '02/05/2018' },
    ],
  };

  render() {
    const { operations } = this.state;
    return (
      <View style={[commonStyles.screen, commonStyles.padding]}>
        {operations.map((operation) => (
          <View 
            key={operation.id} 
            style={[commonStyles.card, commonStyles.padding, commonStyles.marginBottom]}
          >
            <View 
              key={operation.id} 
              style={[commonStyles.row, commonStyles.marginBottomSmall]}
            >
              <Text style={[commonStyles.largeText, commonStyles.flex]}>{operation.id}</Text>
              <Text style={[commonStyles.smallText, commonStyles.lightGrayText]}>{operation.timestamp}</Text>
            </View>
            {statuses[operation.status]}
          </View>
        ))}
      </View>
    );
  }
}