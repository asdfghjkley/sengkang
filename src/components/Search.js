import React from 'react';
import PropTypes from 'prop-types';
import { Image, StyleSheet, View, TextInput, TouchableOpacity, Text } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { observable, computed } from 'mobx';
import { observer } from 'mobx-react';

export class SearchStore {
  @observable isOpen = false;

  @observable text = '';

  @computed get shouldDisplayInput () {
    return this.isOpen || this.text;
  }

  toggle = () => {
    this.isOpen = !this.isOpen;
  }

  open = () => {
    this.isOpen = true;
  }

  close = () => {
    this.isOpen = false;
  }

  edit = (text) => {
    this.text = text;
  };
}

@observer
export class SearchInput extends React.Component {
  static propTypes = {
    searchStore: PropTypes.object.isRequired,
    otherwise: PropTypes.node.isRequired,
  };

  render () {
    return this.props.searchStore.shouldDisplayInput ? (
      <TextInput
        value={this.props.searchStore.text}
        onChangeText={this.props.searchStore.edit}
        style={styles.input}
        autoFocus={true}
        onBlur={this.props.searchStore.close}
        underlineColorAndroid="rgba(0,0,0,0)"
        tintColor="white"
      />
    ) : (
      <Text style={styles.title}>{this.props.otherwise}</Text>
    );
  }
}

@observer
export class SearchButton extends React.Component {
  static propTypes = {
    searchStore: PropTypes.object.isRequired,
  };

  render () {
    return (
      <TouchableOpacity onPress={this.props.searchStore.open}>
        <Icon name="search" color="white" size={24} style={styles.searchIcon}/>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    color: 'white',
    fontSize: 24,
  },
  searchIcon: {
    paddingLeft: 16,
    paddingRight: 16,
  },
  input: {
    color: 'white',
  },
});