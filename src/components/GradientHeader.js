import React from 'react';
import PropTypes from 'prop-types';
import { Image, StyleSheet, View } from 'react-native';
import { Header } from 'react-navigation' ;
import LinearGradient from 'react-native-linear-gradient';
import { shadow } from '../style';

export const GradientHeader = (props) => (
  <View style={styles.container}>
    <LinearGradient colors={['#da1f15', '#78110b']} style={[StyleSheet.absoluteFill, styles.linearGradient]}/>
    <Header {...props} style={styles.header}/>
  </View>
);

const styles = StyleSheet.create({
  container: {
    ...shadow,
    overflow: 'visible',
  },
  linearGradient: {

  },
  header: {
    backgroundColor: 'transparent',
  },
})