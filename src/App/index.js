import React from 'react';
import PropTypes from 'prop-types';
import { App as AppComponent } from './App';
import { drawerStore } from '../services/drawerStore';

export const App = () => {
  return (
    <AppComponent drawerStore={drawerStore}/>
  );
};
