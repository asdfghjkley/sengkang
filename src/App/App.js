import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View } from 'react-native';
import { StackNavigator } from 'react-navigation';
import RNDrawer from 'react-native-drawer'; 
import { observer } from 'mobx-react';
import { navigationStore } from '../services/navigationStore';
import { SplashScreen } from '../SplashScreen';
import { ShipmentsListScreen } from '../ShipmentsListScreen';
import { ShipmentScreen } from '../ShipmentScreen';
import { OperationsHistoryScreen } from '../OperationsHistoryScreen';
import { Drawer } from '../Drawer';

const Navigator = StackNavigator({ 
  SplashScreen: { screen: SplashScreen }, 
  ShipmentsListScreen: { screen: ShipmentsListScreen }, 
  ShipmentScreen: { screen: ShipmentScreen }, 
  OperationsHistoryScreen: { screen: OperationsHistoryScreen }, 
}); 

@observer
export class App extends React.Component {
  static propTypes = {
    drawerStore: PropTypes.object.isRequired,
  };

  handleNavigator = (navigator) => { 
    navigationStore.navigator = navigator;
  }; 

  render () {
    const { drawerStore } = this.props;
    return (
      <RNDrawer
        open={drawerStore.isOpen}
        content={<Drawer drawerStore={drawerStore} navigationStore={navigationStore}/>}
        openDrawerOffset={0.2}
        closedDrawerOffset={-6}
        panOpenMask={50}
        elevation={10}
        type='overlay'
        tapToClose={true}
        styles={drawerStyles}
      >
        <Navigator ref={this.handleNavigator}/>
      </RNDrawer>
    ); 
  }
};

const drawerStyles = {
  drawer: { shadowColor: '#000000', shadowOpacity: 0.8, shadowRadius: 3 },
  main: { paddingLeft: 3 },
}

