import React from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { StyleSheet, Text, View, TouchableOpacity, Button, Image } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { accentColor, primaryColor, commonStyles, textColor, shadow } from '../style';

@observer
export class Drawer extends React.Component {
  static propTypes = {
    drawerStore: PropTypes.object.isRequired,
    navigationStore: PropTypes.object.isRequired,
  };

  handleNavigate = (screen) => () => {
    this.props.navigationStore.navigate(screen);
    this.props.drawerStore.toggle();
  };

  render() {
    const { navigationStore, sessionStore } = this.props;
    return (
      <View style={styles.container}>
        {[
          { screen: 'ShipmentsListScreen', label: 'Shipments list', icon: 'send', }, 
          { screen: 'OperationsHistoryScreen', label: 'Operations history', icon: 'history', }
        ].map(({ screen, label, icon }) => (
          <TouchableOpacity key={screen} onPress={this.handleNavigate(screen)}>
            <View style={[styles.menuItem, navigationStore.currentScreen === screen && styles.activeMenuItem]}>
              <Icon name={icon} color="gray" size={22} style={styles.marginRight}/>
              <Text style={[commonStyles.largeText]}>{label}</Text>
            </View>
          </TouchableOpacity>
        ))}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  menuItem: {
    padding: 16,
    flexDirection: 'row',
    alignItems: 'center',
  },
  activeMenuItem: {
    backgroundColor: accentColor,
  }
});