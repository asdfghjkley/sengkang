import { observable } from 'mobx';

export class ShipmentCounterStore {
  @observable value = 0;

  @observable isStarted = false;

  @observable isPaused = false;

  start = () => {
    this.isStarted = true;
    this.isPaused = false;
  };

  pause = () => {
    this.isStarted = true;
    this.isPaused = true;
  };

  stop = () => {
    this.isStarted = false;
    this.isPaused = false;
    this.value = 0;
  };
}
