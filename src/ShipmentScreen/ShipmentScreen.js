import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View, TouchableOpacity, Button, Image } from 'react-native';
import { commonStyles } from '../style';
import { GradientHeader } from '../components/GradientHeader';
import { SearchStore, SearchButton, SearchInput } from '../components/Search';
import { ShipmentCounterStore } from './ShipmentCounterStore';
import { ShipmentCounter } from './ShipmentCounter';

export const shipmentSearchStore = new SearchStore();

export class ShipmentScreen extends React.Component {
  static propTypes = {
    navigation: PropTypes.any.isRequired,
  };

  static navigationOptions = ({ navigation }) => ({
    title: navigation.state.params.shipmentId,
    header: (props) => <GradientHeader {...props}/>,
    headerTintColor: 'white',
    headerStyle: {
      backgroundColor: 'transparent',
    },
    headerTitleStyle: {
      color: 'white',
    },
    headerTitle: <SearchInput searchStore={shipmentSearchStore} otherwise={navigation.state.params.shipmentId}/>,
    headerRight: <SearchButton searchStore={shipmentSearchStore}/>,
  });

  state = {
    shipment: { 
      id: 'RSQ110293E8902', 
      address: '123 6th St.  Melbourne, FL 32904', 
      timestamp: '02/05/2018',
      items: [{
        id: '111',
        price: '$30',
        description: 'Echo Dot is a voice-controlled device that uses Alexa to play music, control smart home products, provide information, and more.',
        image: 'https://images-na.ssl-images-amazon.com/images/I/41iz5Tw82IL._SY300_QL70_.jpg',
      }],
    },
  };

  constructor () {
    super();
    this.shipmentCounterStore = new ShipmentCounterStore();
    this.shipmentCounterStore.isStarted = true;
    this.shipmentCounterStore.isPaused = true;
    this.shipmentCounterStore.value = 35;
  }

  render() {
    const { shipment } = this.state;
    return (
      <View style={commonStyles.screen}>
        <View style={[commonStyles.flex, commonStyles.padding]}>
          <Text style={[commonStyles.regularText, commonStyles.grayText]}>SHIPPING ADDRESS:</Text>
          <Text style={[commonStyles.regularText, commonStyles.marginBottom]}>{shipment.address}</Text>
          <Text style={[commonStyles.regularText, commonStyles.grayText]}>DELIVERY DATE:</Text>
          <Text style={[commonStyles.regularText, commonStyles.marginBottom]}>{shipment.timestamp}</Text>
          <Text style={[commonStyles.regularText, commonStyles.grayText, commonStyles.marginBottomSmall]}>SHIPMENT ITEMS:</Text>
          {shipment.items.map((item) => (
            <View 
              key={item.id} 
              style={[commonStyles.card, commonStyles.padding, commonStyles.collapseLeft, commonStyles.marginBottom, commonStyles.row]
            }>
              <Image source={{ uri: item.image }} style={styles.image}/>
              <View style={[commonStyles.flex]}>
                <Text style={[commonStyles.flex, commonStyles.regularText, commonStyles.marginBottom]}>{item.description}</Text>
                <Text style={[commonStyles.regularText, commonStyles.bold]}>{item.price}</Text>
              </View>
            </View>
          ))}
        </View>
        <ShipmentCounter counterStore={this.shipmentCounterStore}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  image: {
    width: 100,
    height: 100,
  },
  paddingRightDouble: {
    paddingRight: 64,
  },
});