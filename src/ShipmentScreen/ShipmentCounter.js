import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, TouchableOpacity, Image, Text } from 'react-native';
import { observer } from 'mobx-react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import LinearGradient from 'react-native-linear-gradient';
import { green, yellow, red, accentColor, backgroundColor, borderColor, textColor, subheaderColor, fontSize, shadow } from '../style';

@observer
export class ShipmentCounter extends React.Component {
  static propTypes = {
    counterStore: PropTypes.object.isRequired,
  };

  render() {
    const { counterStore } = this.props;
    const gradient = 
      !counterStore.isStarted ? [red, '#c35657'] :
      !counterStore.isPaused ? [green, '#417544'] :
        [yellow, '#ddd61c'];
    return (
      <View style={styles.container}>
        <LinearGradient colors={gradient} style={styles.callout}>
          <Text style={styles.calloutText}>{counterStore.value}</Text>
        </LinearGradient>
        <LinearGradient colors={['#333333', '#000000']} style={styles.buttons}>
          <TouchableOpacity onPress={counterStore.start}>
            <View style={[styles.button, styles.startButton]}>
              <Icon name="play-arrow" color={green} size={50}/>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={counterStore.pause}>
            <View style={[styles.button, styles.pauseButton]}>
              <Icon name="pause" color={yellow} size={50}/>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={counterStore.stop}>
            <View style={[styles.button, styles.stopButton]}>
              <Icon name="stop" color={red} size={50}/>
            </View>
          </TouchableOpacity>
        </LinearGradient>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    overflow: 'visible',
  },
  buttons: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    backgroundColor: 'black',
    ...shadow,
  },
  button: {
    // flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 8,
    width: 64,
    height: 64,
    borderRadius: 32,
    borderWidth: 2,
  },
  startButton: {
    borderColor: green,
  },
  pauseButton: {
    borderColor: yellow,
  },
  stopButton: {
    borderColor: red,
  },
  callout: {
    position: 'absolute',
    top: -86,
    right: 20,
    width: 64,
    height: 64,
    borderRadius: 32,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: accentColor,
    ...shadow,
  },
  calloutText: {
    fontSize: 24,
  },
});