import React from 'react';
import PropTypes from 'prop-types';
import { navigationStore } from '../services/navigationStore';
import { StyleSheet, View, TouchableOpacity, Image, Dimensions } from 'react-native';

export class SplashScreen extends React.Component {
  static propTypes = {
    navigation: PropTypes.any.isRequired,
  };

  static navigationOptions = {
    header: null,
  };

  handleReady = () => {
    navigationStore.navigate('ShipmentsListScreen');
  };

  componentDidMount () {
    setTimeout(this.handleReady, 1000);
  }

  render() {
    return (
      <View style={styles.container}>
        <Image source={require('./splash.png')} style={styles.image} resizeMode={'contain'}/>
      </View>
    );
  }
}

const { height, width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  image: {
      flex: 1,
      alignSelf: 'stretch',
      width: width,
      height: height,
  },
});