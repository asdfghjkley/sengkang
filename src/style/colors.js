export const primaryColor = '#e31b25';

export const accentColor = '#fffa63';

export const textColor = 'rgba(0,0,0,0.87)';

export const whiteTextColor = 'rgba(255,255,255,0.87)';

export const grayTextColor = 'rgba(100,100,100,0.87)';

export const lightGrayTextColor = 'rgba(150,150,150,0.87)';

export const green = '#6cd971';

export const yellow = '#fdf861';

export const red = '#fd6f70';