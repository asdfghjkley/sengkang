import { StyleSheet } from 'react-native';
import { accentColor, textColor, whiteTextColor, grayTextColor, lightGrayTextColor } from './colors';

export const shadow = {
  shadowColor: 'black',
  shadowOffset: {
    width: 0,
    height: 3,
  },
  shadowOpacity: .19,
  shadowRadius: 6,
  elevation: 4,
};

export const commonStyles = StyleSheet.create({
  row: {
    flexDirection: 'row',
  },
  flex: {
    flex: 1,
  },
  screen: {
    backgroundColor: '#eeeeee',
    flex: 1,
  },
  padding: {
    padding: 16,
  },
  paddingRight: {
    paddingRight: 16,
  },
  paddingLeft: {
    paddingLeft: 16,
  },
  collapseLeft: {
    paddingLeft: 0,
  },
  marginBottom: {
    marginBottom: 16,
  },
  marginBottomSmall: {
    marginBottom: 8,
  },
  marginLeft: {
    marginLeft: 16,
  },
  marginLeftSmall: {
    marginLeft: 8,
  },
  marginRight: {
    marginRight: 16,
  },
  marginRightSmall: {
    marginRight: 8,
  },
  card: {
    borderRadius: 3,
    backgroundColor: 'white',
    overflow: 'visible',
    ...shadow,
  },
  regularText: {
    fontSize: 11,
    color: textColor,
  },
  largeText: {
    fontSize: 14,
    color: textColor,
  },
  smallText: {
    fontSize: 10,
    color: textColor,
  },
  whiteText: {
    color: whiteTextColor,
  },
  grayText: {
    color: grayTextColor,
  },
  lightGrayText: {
    color: lightGrayTextColor,
    fontWeight: '100',
  },
  bold: {
    fontWeight: 'bold',
  },
  overflowVisible: {
    overflow: 'visible',
  },
  highlight: {
    backgroundColor: accentColor,
  }
});